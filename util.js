var users = []

function getUsers() {
  return new Promise (
    function (resolve, reject) {

      var r = new XMLHttpRequest()
      r.open('GET', 'https://jsonplaceholder.typicode.com/users', true)
      r.onload = function (e) {
        if (r.readyState === 4) {
          if (r.status === 200) {
            users = JSON.parse(r.responseText)
            resolve()
          } else {
            console.error(r.statusText)
          }
        }
      };
      r.onerror = function (e) {
        console.error(r.statusText)
      };
      r.send(null)

    }
  )
}

const e = React.createElement

function ListOfUsers (arr) {
  return e('ol', null, arr.map(function(elt) {
    return e('li',{'key':elt.id},
      e('span',null,elt.name),
      e('span',{'className': 'text-gray-700 ml-2'},'@'+elt.username)) 
  }))
}

function filterUsers(str, arr) {
    return arr.filter(function(x) {
      const titleAndName = x.name
      const nameArr = titleAndName.split(" ")
      if (nameArr[0] === 'Mrs.') {
        nameArr.splice(0,1)
      }
      const name = nameArr[0]
      const surname = nameArr[1]
      return name.toLowerCase().startsWith(str) || surname.toLowerCase().startsWith(str)
    })
}

function addInputHandlerToSearchBox() {
  var sbox = document.getElementById('searchbox')
  sbox.addEventListener('input', x => {
    const filtered = filterUsers(event.target.value, users)
    if (filtered) {
      var c = document.getElementById("users_container")
      ReactDOM.render(ListOfUsers(filtered), c);
    }
  })
}
